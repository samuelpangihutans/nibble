<?php
$mysqli=new mysqli('localhost','user','','nibble_front_end_test');

?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
    <title>Nibble</title>

  </head>


  <body>
  <!-- navigation Bar -->
  <nav class="navbar navbar-expand-lg navbar-dark" style="background-color:black;">
  <img class="d-inline p-2" style="width:5%" class="nav-link" href="#" src="images/logo.png" alt="">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto col-md-5 mx-auto">
    
      <?php
      $query="select menu.id_menu as id_menu, menu.name as menu_name , count(submenu.id_subMenu) as jumlah_subMenu FROM menu left join submenu on menu.id_menu = submenu.id_menu
      group by menu.id_menu ORDER by menu.id_menu ";
      $result = $mysqli -> query($query);
        while($r = mysqli_fetch_assoc($result)) {
          $rows[] = $r;
      }
      $query2="select menu.id_menu as id_menu, menu.name as menu_name , submenu.id_subMenu as id_subMenu ,submenu.name as subMenu_name  FROM menu left join submenu on menu.id_menu = submenu.id_menu ORDER by menu.id_menu";
      $result2 = $mysqli -> query($query2);
        while($r2 = mysqli_fetch_assoc($result2)) {
          $rows2[] = $r2;
      }
        
        for($i=0;$i<count($rows);$i++){
          if($rows[$i]['jumlah_subMenu']==0){
            echo '<li class="nav-item">
            <a class="nav-link" href="#">'.$rows[$i]["menu_name"].'<span class="sr-only">(current)</span></a>
          </li>';
          }
          else{
            echo '
            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'
            .$rows[$i]["menu_name"].'
            </a><div class="dropdown-menu" aria-labelledby="navbarDropdown">';
            for($j=0;$j<count($rows2);$j++){
              if($rows[$i]['id_menu']==$rows2[$j]['id_menu']){
                echo '<a class="dropdown-item" href="#">'.$rows2[$j]['subMenu_name'].'</a>';
              }
            }
            echo '</div></li>';
          }
        }
      ?>

      <!-- Tanpa Database  -->
      <!-- 
      <li class="nav-item">
        <a class="nav-link" href="#">HOME <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">ABOUT</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">PRICING</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">GALLERY</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          PAGES
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Pages 1</a>
          <a class="dropdown-item" href="#">Pages 2</a>
          <a class="dropdown-item" href="#">Pages 3</a>
        </div>

      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          BLOG
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">BLOG</a>
          <a class="dropdown-item" href="#">SINGLE-BLOG</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">CONTACT</a>
      </li> -->
    </ul>



    <div style="background-color:red"class="d-none d-lg-block p-3">
         <a id="join" class="nav-item" href="#">JOIN US</a>
    </div>
    
      

  </div>
</nav>

 <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  
    <div class="carousel-inner" role="listbox">
      <!-- Slide One - Set the background image for this slide in the line below -->
      <div class="carousel-item active" style="background-image:url('images/b1.jpg')">
        <div class="carousel-caption  mb-5">
          <h2>Build Up Your</h2>
          <h1 class="display-4">Strength</h1>
          <h3 class="lead">Build Your Body and Fitness with Professional Touch </h3>
          <div class="mr-auto col-md-5 mx-auto" style="background-color:red;width:10%;height:10%;padding:1.5%">
            <a id="join" class="nav-item" href="#">JOIN US</a>
          </div>
        </div>
      </div>
      <!-- Slide Two - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image: url('images/b2.jpg')">
        <div class="carousel-caption  mb-5">
          <h2>Build Up Your</h2>
          <h1 class="display-4">Strength</h1>
          <h3 class="lead">Build Your Body and Fitness with Professional Touch </h3>
          <div class="mr-auto col-md-5 mx-auto" style="background-color:red;width:10%;height:10%;padding:1.5%">
            <a id="join" class="nav-item" href="#">JOIN US</a>
          </div>
        </div>
      </div>
      <!-- Slide Three - Set the background image for this slide in the line below -->
      <div class="carousel-item" style="background-image:url('images/b3.jpg')">
        <div class="carousel-caption  mb-5">
          <h2>Build Up Your</h2>
          <h1 class="display-4">Strength</h1>
          <h3 class="lead">Build Your Body and Fitness with Professional Touch </h3>
          <div class="mr-auto col-md-5 mx-auto" style="background-color:red;width:10%;height:10%;padding:1.5%">
            <a id="join" class="nav-item" href="#">JOIN US</a>
          </div>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>

<!-- swipe start -->
<script src='//cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js'></script>
<script type="text/javascript">
    $(".carousel").swipe({
        swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
            if (direction == 'left') $(this).carousel('next');
            if (direction == 'right') $(this).carousel('prev');
        },
        allowPageScroll: "vertical" 
    });
</script>
